
let btn = document.getElementById("btn-click");
let section = document.getElementById("content");
btn.addEventListener("click", () => {
  let p = document.createElement("p");
  p.innerText = "New Paragraph";
  section.append(p);
});

let btnInputCreate = document.createElement("button");
btnInputCreate.setAttribute("id", "btn-input-create");
btnInputCreate.innerText = "Create btn";
section.append(btnInputCreate);
btnInputCreate.addEventListener("click", () => {
  let input = document.createElement("input");
  input.setAttribute("type", "text");
  input.setAttribute("placeholder", "Enter your text");
  input.setAttribute("name", "Input");
  btnInputCreate.after(input)
});
